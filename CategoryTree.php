<?php
// include the oop Class
require_once'CategoryItemCounter.php';
?>
<!DOCTYPE html>
<html>
  <head>
    <title>Category Tree List with Item Number</title>
    <style type="text/css">
      .conainer {
        text-align:left;
        margin:0 auto;
      }
      ul li {
        list-style-type:none;
      }
    </style>
  </head>
  <body>
    <div class="conainer">
      <h3>Task 2 Output</h3>
        <ul>
         <?php
          $cic_object = new CategoryItemCounter();
          $Category_list = $cic_object->parentCategoryList();
          $i = 0;
          foreach ($Category_list as $cat_row) {
            ++$i;
          ?>
          <li>
            <?php 
            $child_category_list = $cic_object->childCategoryListByParent($cat_row['id']);
            $parent_total_item = $cic_object->itemCountByParentCategory($cat_row['id']);
            echo htmlentities($cat_row['category_name']). "  (".$parent_total_item.")";
            ?>
              
              <?php 
              if(isset($child_category_list) && !empty($child_category_list)){
              foreach ($child_category_list as $child_cat_row) {
                $grant_child_category_list = $cic_object->childCategoryListByParent($child_cat_row);
                $child_category_name = $cic_object->categoryNameById($child_cat_row);
                if(isset($grant_child_category_list) && !empty($grant_child_category_list)){
                  $child_total_item = $cic_object->itemCountByCategory($grant_child_category_list);
                } else {
                  $child_total_item = $cic_object->itemCountByCategory($child_cat_row);
                }
              ?>
              <ul>
                <li>
                  <?php echo htmlentities($child_category_name). "  (".$child_total_item.")";?>

                  <?php if(isset($grant_child_category_list) && !empty($grant_child_category_list)){
                  foreach ($grant_child_category_list as $gchild_cat_row) {
                    $gchild_category_name = $cic_object->categoryNameById($gchild_cat_row);
                    $gchild_total_item = $cic_object->itemCountByCategory($gchild_cat_row);
                  ?>
                  <ul>
                    <li>
                      <?php echo htmlentities($gchild_category_name). "  (".$gchild_total_item.")";?>
                        
                      </li>
                  </ul>
                   <?php 
                   }
                  } 
                ?>
                </li>
              </ul>
            <?php 
              }
            } 
            ?>
          </li>
        <?php } ?>
       </ul>
  </div>
  </body>
</html>