<?php
session_start();
define('DB_SERVER','localhost');
define('DB_USER','root');
define('DB_PASS' ,'');
define('DB_NAME', 'ecommerce');


class CategoryItemCounter {

	public $output = array();

	public function __construct() {
		$con = mysqli_connect(DB_SERVER,DB_USER,DB_PASS,DB_NAME);
		$this->dbh = $con;
		/*
		* Check connection error
		*/
		if (mysqli_connect_errno()) {
			echo "Failed to connect to MySQL: " . mysqli_connect_error();
		}
	}

	//Category List by no of item
	public function categoryList() {
		$result = mysqli_query($this->dbh,"select c.name, count(icr.categoryId) as totalItem from category as c left join item_category_relations as icr on c.id = icr.categoryId  group by c.id order by totalItem desc;");
		return $result;
	}

	// Get all parent Category List

	public function parentCategoryList() {
		$this->output = array();
		$sql = mysqli_query($this->dbh,"select distinct(c.id), c.name from category as c right join catetory_relations as cl on c.id = cl.ParentcategoryId  order by c.Priority asc");
		while ($row = mysqli_fetch_array($sql)){
			$this->output[] = array(
				'id' => $row['id'],
				'category_name' => $row['name'] 
			);
		}
		$output = $this->output;
		$this->output = array();
		return $output;
	}

	public function childCategoryListByParent($parent_id) {
		$this->output = array();
		$sql = mysqli_query($this->dbh,"select c.id, c.name from category as c left join catetory_relations as cl on c.id = cl.categoryId where cl.ParentcategoryId = $parent_id");
		while ($row = mysqli_fetch_array($sql)){
			$this->output[$row['id']] = $row['id'];
		}
		$output = $this->output;
		$this->output = array();
		return $output;
	}

	public function itemCountByParentCategory($cat_id){
		$child_cat_list = $this->childCategoryListByParent($cat_id);
		$grand_child_count = $count = 0;
		if(isset($child_cat_list) && !empty($child_cat_list)){
			foreach ($child_cat_list as $child_cat_row) {
				$grand_child_list = $this->childCategoryListByParent($child_cat_row);
				$grand_child_count += $this->itemCountByCategory($grand_child_list);
			}
		
			$result = mysqli_query($this->dbh,"select count(icr.ItemNumber) as totalItem from item_category_relations as icr  where icr.categoryId IN (".implode(',', $child_cat_list).")");
			$row = mysqli_fetch_assoc($result);
			$count = $row['totalItem'];
		}
		$count += $grand_child_count;
		return $count;
	}

	public function itemCountByCategory($cat_list){
		if(!is_array($cat_list)){
			$cat_list = (array) $cat_list;
		}
		$count = 0;
		if(isset($cat_list) && !empty($cat_list)){
			$result = mysqli_query($this->dbh,"select count(icr.ItemNumber) as totalItem from item_category_relations as icr  where icr.categoryId IN (".implode(',', $cat_list).")");
			$row = mysqli_fetch_assoc($result);
			$count = $row['totalItem'];
		}
		return $count;
	}

	public function categoryNameById($categoryId){
		$result = mysqli_query($this->dbh,"select c.name from category as c  where c.id = $categoryId");
		$row = mysqli_fetch_assoc($result);
		$category_name = $row['name'];
		return $category_name;
	}

	//Category List by no of item
	public function categoryTreeList() {
		$result = mysqli_query($this->dbh,"select c.name, count(icr.categoryId) as totalItem from category as c left join item_category_relations as icr on c.id = icr.categoryId  group by c.id order by totalItem;");
		return $result;
	}
}

?>