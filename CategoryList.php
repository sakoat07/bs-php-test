<?php
// include the oop Class
require_once'CategoryItemCounter.php';
?>
<!DOCTYPE html>
<html>
  <head>
    <title>Category Tabular List with Item Number</title>
    <style type="text/css">
      .conainer {
        text-align:left;
        margin:0 auto;
      }
    </style>
  </head>
  <body>
    <div class="conainer">
      <h3>Task 1 Output</h3>
      <table id="mytable" class="table table-bordred table-striped">
        <thead>
          <th style="text-align:left;font-weight:bold;">Category Name</th>
          <th style="text-align:center;font-weight:bold;">Total Items</th>
        </thead>
        <tbody>
         <?php
          $cic_object = new CategoryItemCounter();
          $sql = $cic_object->categoryList();
          
          while($row = mysqli_fetch_array($sql)) {
          ?>
            <tr>
              <td style="text-align:left;"><?php echo htmlentities($row['name']);?></td>
              <td style="text-align:center;"><?php echo htmlentities($row['totalItem']);?></td>
            </tr>
        <?php } ?>
        </tbody>
      </table>
  </div>
  </body>
</html>